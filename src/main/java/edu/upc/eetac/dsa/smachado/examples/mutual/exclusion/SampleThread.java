package edu.upc.eetac.dsa.smachado.examples.mutual.exclusion;

public class SampleThread implements Runnable {
	private static CommonAccess common = new CommonAccess();

	public void run() {

		for (int i = 0; i < 1; i++) {
			System.out.println(Thread.currentThread().getName() + " " + common);
			common.longOperation();
		}

	}

}
