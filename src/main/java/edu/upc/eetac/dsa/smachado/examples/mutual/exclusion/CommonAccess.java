package edu.upc.eetac.dsa.smachado.examples.mutual.exclusion;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonAccess {
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

	public synchronized void longOperation() {
		System.out.println(Thread.currentThread().getName() + " in at "
				+ sdf.format(new Date(System.currentTimeMillis())));

		for (int i = 0; i < 100; i++)
			System.out.println("\t" + Thread.currentThread().getName()
					+ " -> Revolution #" + (i + 1));

		System.out.println(Thread.currentThread().getName() + " out at "
				+ sdf.format(new Date(System.currentTimeMillis())));
	}
}
