package edu.upc.eetac.dsa.smachado.examples.mutual.exclusion;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		for (int i = 0; i < 3; i++) {
			new Thread(new SampleThread(), "SampleThread #" + i).start();
		}

	}
}
